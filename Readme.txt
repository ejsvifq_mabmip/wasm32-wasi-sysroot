This is a wasm32-wasi sysroot. Built by the fast_io library's author

Unix Timestamp:1659225717.465762153
UTC:2022-07-30T23:51:07.095038797Z

fast_io:
https://github.com/cppfastio/fast_io.git

build	:	x86_64-linux-gnu
host	:	wasm32-wasi
